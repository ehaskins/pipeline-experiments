#/usr/bin/env bash

set -eEu

rm -rf output
mkdir -p output

cp ../b/output/* output
cp ../c/output/* output
cp ../d/output/* output
