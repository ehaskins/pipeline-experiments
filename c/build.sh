#/usr/bin/env bash

set -eEu

sleep 5

rm -rf output
mkdir -p output

cp src.txt output/out.txt
